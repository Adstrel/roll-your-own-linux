# Roll Your Own Linux
Set of helpful scripts used for automating the building of a linux system from source/scratch.

# Build instructions
This project requires ```sudo```. Do not run random scripts from the internet with ```sudo```, including this one. If you want to try using this project, prefer to try it in a virtual machine.

This project was tested on Ubuntu 20.04 and Ubuntu 22.04. These packages need to be installed: ```gcc g++ sed awk grep wget make tar findutils```

Run ```make help``` to show a list of possible commands.
