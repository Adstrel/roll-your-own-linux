.PHONY: nothing help all build download toolchain first_tmpsystem

nothing:

help:
	@ echo "make help\t\tDisplay this messege"
	@ echo "make all\t\tDownload and build all packages"
	@ echo "make download\t\tDownload all packages"
	@ echo "make build\t\tBuild all stages"
	@ echo "make toolchain\t\tBuild the first stage - the crosscompiling toolchain"
	@ echo "make first_tmpsystem\tBuild the second stage - temporary tools before chrooting"
	@ echo "make chroot_tmpsystem\tBuild the third stage - temporary tools after chrooting"
	@ echo "make system\t\tBuild the last stage - final version of all the tools in chroot"

all: download build

build: toolchain first_tmpsystem chroot_tmpsystem system

download:
	./scripts/download_packages.sh

toolchain:
	./scripts/toolchain.sh

first_tmpsystem:
	./scripts/first_tmpsystem.sh

chroot_tmpsystem:
	# Be careful about code running before chrooting
	sudo ./scripts/chroot_tmpsystem.sh

system:
	# Be careful about code running before chrooting
	sudo ./scripts/system.sh

