tar xf $PKG_DIR/gettext/gettext-0.21.tar.xz

cd gettext-0.21

mkdir -v build
cd build

../configure --disable-shared

make -j$(nproc)
cp -v gettext-tools/src/{msgfmt,msgmerge,xgettext} /usr/bin

cd ../..
rm -rf gettext-0.21

