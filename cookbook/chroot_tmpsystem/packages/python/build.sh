tar xf $PKG_DIR/python/Python-3.9.6.tar.xz

cd Python-3.9.6

mkdir -v build
cd build

../configure --prefix=/usr   \
             --enable-shared \
             --without-ensurepip

make -j$(nproc)
make install -j1

cd ../..
rm -rf Python-3.9.6

