tar xf $PKG_DIR/bison/bison-3.7.6.tar.xz

cd bison-3.7.6

mkdir -v build
cd build

../configure --prefix=/usr \
             --docdir=/usr/share/doc/bison-3.7.6

make -j$(nproc)
make install -j1

cd ../..
rm -rf bison-3.7.6

