tar xf $PKG_DIR/gcc/gcc-11.2.0.tar.xz

cd gcc-11.2.0

ln -s gthr-posix.h libgcc/gthr-default.h

mkdir -v build
cd build

../libstdc++-v3/configure           \
    CXXFLAGS="-g -O2 -D_GNU_SOURCE" \
    --prefix=/usr                   \
    --disable-multilib              \
    --disable-nls                   \
    --host=x86_64-lfs-linux-gnu     \
    --disable-libstdcxx-pch

make -j$(nproc)
make install -j1

cd ../..
rm -rf gcc-11.2.0

