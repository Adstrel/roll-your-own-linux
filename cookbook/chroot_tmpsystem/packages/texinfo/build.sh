tar xf $PKG_DIR/texinfo/texinfo-6.8.tar.xz

cd texinfo-6.8

sed -e 's/__attribute_nonnull__/__nonnull/' \
        -i gnulib/lib/malloc/dynarray-skeleton.c

mkdir -v build
cd build

../configure --prefix=/usr

make -j$(nproc)
make install -j1

cd ../..
rm -rf texinfo-6.8

