tar xf $PKG_DIR/bzip2/bzip2-1.0.8.tar.gz
cd bzip2-1.0.8

# This patch ensures documentation will be installed
patch -Np1 -i ../bzip2-1.0.8-install_docs-1.patch

# Ensure symlinks are relative
sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile

# We will install dynamic library libbz2.so and link the binaries against it
make -f Makefile-libbz2_so
make clean

make -j$(nproc)
make PREFIX=/usr install -j1

# Install the shared library
cp -av libbz2.so.* /usr/lib
ln -sv libbz2.so.1.0.8 /usr/lib/libbz2.so

cp -v bzip2-shared /usr/bin/bzip2
for i in /usr/bin/{bzcat,bunzip2}; do
    ln -sfv bzip2 $i
done

# We don't want a static library
rm -fv /usr/lib/libbz2.a

cd ..
rm -rf bzip2-1.0.8

