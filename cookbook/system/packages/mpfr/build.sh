tar xf $PKG_DIR/mpfr/mpfr-4.1.0.tar.xz
cd mpfr-4.1.0

mkdir -v build
cd build

../configure --prefix=/usr        \
             --disable-static     \
             --enable-thread-safe \
             --docdir=/usr/share/doc/mpfr-4.1.0

make -j$(nproc)
make html -j $(nproc)

make check -j$(nproc)

make install
make install-html

cd ../..
rm -rf mpfr-4.1.0

