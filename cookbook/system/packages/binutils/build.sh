tar xf $PKG_DIR/binutils/binutils-2.37.tar.xz
cd binutils-2.37

mkdir -v build
cd build

../configure --prefix=/usr       \
             --enable-gold       \
             --enable-ld=default \
             --enable-plugins    \
             --enable-shared     \
             --disable-werror    \
             --enable-64-bit-bdf \
             --with-system-zlib

make tooldir=/usr -j$(nproc)
# TODO: Have a list of ignored tests, instead of ignoring all of them
make -k check -j$(nproc) || true
make tooldir=/usr install -j1

rm -fv /usr/lib/lib{bfd,ctf,ctf-nobfd,opcodes}.a

cd ../..
rm -rf binutils-2.37

