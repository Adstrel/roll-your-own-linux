tar xf $PKG_DIR/acl/acl-2.3.1.tar.xz
cd acl-2.3.1

mkdir -v build
cd build

../configure --prefix=/usr    \
             --disable-static \
             --docdir=/usr/share/doc/acl-2.3.1

make -j$(nproc)
make install -j1

cd ../..
rm -rf acl-2.3.1

