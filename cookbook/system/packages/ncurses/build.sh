tar xf $PKG_DIR/ncurses/ncurses-6.2.tar.gz
cd ncurses-6.2

mkdir -v build
cd build

../configure --prefix=/usr               \
            --mandir=/usr/share/man      \
            --with-shared                \
            --without-debug              \
            --without-normal             \
            --with-cxx-shared            \
            --enable-pc-files            \
            --enable-widec               \
            --with-pkg-config-libdir=/usr/lib/pkgconfig

make -j$(nproc)
make DESTDIR=$PWD/dest install -j1
install -vm755 dest/usr/lib/libncursesw.so.6.2 /usr/lib
rm -v dest/usr/lib/libncursesw.so.6.2
cp -av dest/* /

for lib in ncurses form panel menu ; do
    rm -vf                    /usr/lib/lib${lib}.so
    echo "INPUT(-l${lib}w)" > /usr/lib/lib${lib}.so
    ln -sfv ${lib}w.pc        /usr/lib/pkgconfig/${lib}.pc
done

rm -vf                     /usr/lib/libcursesw.so
echo "INPUT(-lncursesw)" > /usr/lib/libcursesw.so
ln -sfv libncurses.so      /usr/lib/libcurses.so

cd ../..
rm -rf ncurses-6.2

