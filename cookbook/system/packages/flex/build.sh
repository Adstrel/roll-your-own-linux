tar xf $PKG_DIR/flex/flex-2.6.4.tar.gz
cd flex-2.6.4

mkdir -v build
cd build

../configure --prefix=/usr    \
             --disable-static \
             --docdir=/usr/share/doc/flex-2.6.4

make -j$(nproc)
make check -j$(nproc)
make install -j1

# Make a symlink for flex's predecessor "lex"
ln -sv flex /usr/bin/lex

cd ../..
rm -rf flex-2.6.4

