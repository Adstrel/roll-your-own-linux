tar xf $PKG_DIR/m4/m4-1.4.19.tar.xz
cd m4-1.4.19

mkdir -v build
cd build

../configure --prefix=/usr

make -j$(nproc)
make check -j$(nproc)
make install -j1

cd ../..
rm -rf m4-1.4.19

