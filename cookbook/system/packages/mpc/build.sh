tar xf $PKG_DIR/mpc/mpc-1.2.1.tar.gz
cd mpc-1.2.1

mkdir -v build
cd build

../configure --prefix=/usr    \
             --disable-static \
             --docdir=/usr/share/doc/mpc-1.2.1

make -j$(nproc)
make html -j$(nproc)

make check -j$(nproc)

make install
make install-html

cd ../..
rm -rf mpc-1.2.1

