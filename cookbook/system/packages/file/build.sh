tar xf $PKG_DIR/file/file-5.40.tar.gz
cd file-5.40

mkdir -v build
cd build

../configure --prefix=/usr
make -j$(nproc)
make check -j$(nproc)
make install -j1

cd ../..
rm -rf file-5.40

