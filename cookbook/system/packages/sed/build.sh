tar xf $PKG_DIR/sed/sed-4.8.tar.xz
cd sed-4.8

mkdir -v build
cd build

../configure --prefix=/usr

make -j$(nproc)
make html -j$(nproc)

chown -Rv tester .
su tester -c "PATH=$PATH make check"

make install -j1
install -d -m755 /usr/share/doc/sed-4.8
install -m644 doc/sed.html /usr/share/doc/sed-4.8

cd ../..
rm -rf sed-4.8

