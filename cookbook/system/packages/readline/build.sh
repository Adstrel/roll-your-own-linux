tar xf $PKG_DIR/readline/readline-8.1.2.tar.gz
cd readline-8.1.2

# Disable renaming the old libraries when reinstalling
sed -i '/MV.*old/d' Makefile.in
sed -i '/{OLDSUFF}/c:' support/shlib-install

mkdir -v build
cd build

../configure --prefix=/usr    \
             --disable-static \
             --with-curses    \
             --docdir=/usr/share/doc/readline-8.1.2

make -j$(nproc) SHLIB_LIBS="-lncursesw"
make SHLIB_LIBS="-lcursesw" install -j1

cd ../..
rm -rf readline-8.1.2

