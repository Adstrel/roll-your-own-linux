tar xf $PKG_DIR/libcap/libcap-2.65.tar.xz
cd libcap-2.65

# No static libraries
sed -i '/install -m.*STA/d' libcap/Makefile

make prefix=/usr lib=lib
make test
make prefix=/usr lib=lib install

cd ..
rm -rf libcap-2.65

