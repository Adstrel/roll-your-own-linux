tar xf $PKG_DIR/gmp/gmp-6.2.1.tar.xz
cd gmp-6.2.1

mkdir -v build
cd build

../configure --prefix=/usr    \
             --enable-cxx     \
             --disable-static \
             --docdir=/usr/share/doc/gmp-6.2.1

make -j$(nproc)
make html -j$(nproc)

make check -j$(nproc) 2>&1 | tee gmp-check-log
awk '/# PASS:/{total += $3} ; END{print total}' gmp-check-log

make install
make install-html

cd ../..
rm -rf gmp-6.2.1

