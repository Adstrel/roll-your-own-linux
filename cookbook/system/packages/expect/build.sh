tar xf $PKG_DIR/expect/expect5.45.4.tar.gz
cd expect5.45.4

mkdir -v build
cd build

../configure --prefix=/usr           \
             --with-tcl=/usr/lib     \
             --enable-shared         \
             --mandir=/usr/share/man \
             --with-tclinclude=/usr/include

make -j$(nproc)
make test -j$(nproc)
make install -j1
ln -svf expect5.45.4/libexpect5.45.4.so /usr/lib

cd ../..
rm -rf expect5.45.4

