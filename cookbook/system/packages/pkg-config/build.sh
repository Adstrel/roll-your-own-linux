tar xf $PKG_DIR/pkg-config/pkg-config-0.29.2.tar.gz
cd pkg-config-0.29.2

mkdir -v build
cd build

../configure --prefix=/usr        \
             --with-internal-glib \
             --disable-host-tool  \
             --docdir=/usr/share/doc/pkg-config-0.29.2

make -j$(nproc)
make check -j$(nproc)
make install -j1

cd ../..
rm -rf pkg-config-0.29.2

