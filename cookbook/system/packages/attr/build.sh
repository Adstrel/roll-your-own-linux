tar xf $PKG_DIR/attr/attr-2.5.1.tar.xz
cd attr-2.5.1

mkdir -v build
cd build

../configure --prefix=/usr     \
             --disable-static  \
             --sysconfdir=/etc \
             --docdir=/usr/share/doc/attr-2.5.1

make -j$(nproc)
make check -j$(nproc)
make install -j1

cd ../..
rm -rf attr-2.5.1

