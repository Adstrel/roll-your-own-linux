tar xf $PKG_DIR/zstd/zstd-1.5.0.tar.gz
cd zstd-1.5.0

make -j$(nproc)
make check -j$(nproc)
make prefix=/usr install -j1

# Remove the static library
rm -v /usr/lib/libzstd.a

cd ..
rm -rf zstd-1.5.0

