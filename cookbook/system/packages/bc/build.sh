tar xf $PKG_DIR/bc/bc-6.0.1.tar.xz
cd bc-6.0.1

mkdir -v build
cd build

CC=gcc ../configure --prefix=/usr -G -O3

make -j$(nproc)
make test -j$(nproc)
make install -j1

cd ../..
rm -rf bc-6.0.1

