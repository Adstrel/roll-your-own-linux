tar xf $PKG_DIR/zlib/zlib-1.2.13.tar.xz
cd zlib-1.2.13

mkdir -v build
cd build

../configure --prefix=/usr

make -j$(nproc)
make check
make install -j1

rm -fv /usr/lib/libz.a

cd ../..
rm -rf zlib-1.2.13

