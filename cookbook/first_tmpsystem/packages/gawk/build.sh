tar xf $PKG_DIR/gawk/gawk-5.1.0.tar.xz
cd gawk-5.1.0

# Some programs are not needed
sed -i 's/extras//' Makefile.in

mkdir -v build
cd build

../configure --prefix=/usr   \
             --host=$LFS_TGT \
             --build=$(../config.guess)

make -j$(nproc)
make DESTDIR=$LFS install -j1

cd ../..
rm -rf gawk-5.1.0

