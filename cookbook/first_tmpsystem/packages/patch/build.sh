tar xf $PKG_DIR/patch/patch-2.7.6.tar.xz
cd patch-2.7.6

mkdir -v build
cd build

../configure --prefix=/usr   \
             --host=$LFS_TGT \
             --build=$(../build-aux/config.guess)

make -j$(nproc)
make DESTDIR=$LFS install -j1

cd ../..
rm -rf patch-2.7.6

