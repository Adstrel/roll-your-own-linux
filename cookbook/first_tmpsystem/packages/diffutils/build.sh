tar xf $PKG_DIR/diffutils/diffutils-3.8.tar.xz
cd diffutils-3.8

mkdir -v build
cd build

../configure        \
    --prefix=/usr   \
    --host=$LFS_TGT

make -j$(nproc)
make DESTDIR=$LFS install -j1

cd ../..
rm -rf diffutils-3.8

