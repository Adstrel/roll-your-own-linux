tar xf $PKG_DIR/ncurses/ncurses-6.2.tar.gz
cd ncurses-6.2

# We should use gawk, not mawk
sed -i s/mawk// configure

mkdir -v build_tic
cd build_tic

../configure
make -C include
make -C progs tic

cd ..
mkdir -v build
cd build

../configure --prefix=/usr               \
            --host=$LFS_TGT              \
            --build=$(../config.guess)   \
            --mandir=/usr/share/man      \
            --with-manpage-format=normal \
            --with-shared                \
            --without-debug              \
            --without-ada                \
            --without-normal             \
            --enable-widec

make -j$(nproc)
make DESTDIR=$LFS TIC_PATH=$(pwd)/../build_tic/progs/tic install -j1
echo "INPUT(-lncursesw)" > $LFS/usr/lib/libncurses.so

cd ../..
rm -rf ncurses-6.2

