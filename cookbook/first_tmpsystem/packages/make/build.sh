tar xf $PKG_DIR/make/make-4.3.tar.gz
cd make-4.3

mkdir -v build
cd build

../configure --prefix=/usr   \
             --without-guile \
             --host=$LFS_TGT \
             --build=$(../build-aux/config.guess)

make -j$(nproc)
make DESTDIR=$LFS install -j1

cd ../..
rm -rf make-4.3

