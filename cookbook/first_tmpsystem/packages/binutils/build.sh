tar xf $PKG_DIR/binutils/binutils-2.37.tar.xz
cd binutils-2.37

mkdir -v build
cd build

../configure                   \
    --prefix=/usr              \
    --build=$(../config.guess) \
    --host=$LFS_TGT            \
    --disable-nls              \
    --enable-shared            \
    --disable-werror

make -j$(nproc)
make DESTDIR=$LFS install -j1

# One libary might have got linked against zlib from host distribution
install -vm755 libctf/.libs/libctf.so.0.0.0 $LFS/usr/lib

cd ../..
rm -rf binutils-2.37

