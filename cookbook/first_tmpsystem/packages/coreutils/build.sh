tar xf $PKG_DIR/coreutils/coreutils-8.32.tar.xz
cd coreutils-8.32

mkdir -v build
cd build

../configure --prefix=/usr                  \
    --host=$LFS_TGT                         \
    --build=$(../build-aux/config.guess)    \
    --enable-install-program=hostname       \
    --enable-no-install-program=kill,uptime

make -j$(nproc)
make DESTDIR=$LFS install -j1

# mv -v $LFS/usr/bin/chroot               $LFS/usr/sbin
mkdir -pv $LFS/usr/share/man/man8
mv -v $LFS/usr/share/man/man1/chroot.1  $LFS/usr/share/man/man8/chroot.8
sed -i 's/"1"/"8"/'                     $LFS/usr/share/man/man8/chroot.8

cd ../..
rm -rf coreutils-8.32

