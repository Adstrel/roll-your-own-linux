tar xf $PKG_DIR/tar/tar-1.34.tar.xz
cd tar-1.34

mkdir -v build
cd build

../configure --prefix=/usr                     \
             --host=$LFS_TGT                   \
             --build=$(../build-aux/config.guess)

make -j$(nproc)
make DESTDIR=$LFS install -j1

cd ../..
rm -rf tar-1.34

