tar xf $PKG_DIR/file/file-5.40.tar.gz
cd file-5.40

mkdir -v build_tmp
cd build_tmp

../configure --disable-bzlib      \
             --disable-libseccomp \
             --disable-xzlib      \
             --disable-zlib

make -j$(nproc)

cd ..
mkdir -v build
cd build

../configure            \
    --prefix=/usr       \
    --host=$LFS_TGT     \
    --build=$(../config.guess)

make FILE_COMPILE=$(pwd)/../build_tmp/src/file -j$(nproc)
make DESTDIR=$LFS install -j1

cd ../..
rm -rf file-5.40

