tar xf $PKG_DIR/m4/m4-1.4.19.tar.xz
cd m4-1.4.19

mkdir -v build
cd build

../configure --prefix=/usr  \
    --host=$LFS_TGT         \
    --build=$(build-aux/config.guess)

make -j$(nproc)
make DESTDIR=$LFS install -j1

cd ../..
rm -rf m4-1.4.19

