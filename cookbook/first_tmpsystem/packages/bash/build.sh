tar xf $PKG_DIR/bash/bash-5.1.8.tar.gz
cd bash-5.1.8

mkdir -v build
cd build

../configure --prefix=/usr              \
    --build=$(../support/config.guess)  \
    --host=$LFS_TGT                     \
    --without-bash-malloc               \
    bash_cv_getcwd_malloc=yes

make -j$(nproc)
make DESTDIR=$LFS install -j1
ln -sv bash $LFS/bin/sh

cd ../..
rm -rf bash-5.1.8

