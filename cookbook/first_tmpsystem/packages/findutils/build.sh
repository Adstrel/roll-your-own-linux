tar xf $PKG_DIR/findutils/findutils-4.8.0.tar.xz
cd findutils-4.8.0

mkdir -v build
cd build

../configure --prefix=/usr                  \
            --localstatedir=/var/lib/locate \
            --host=$LFS_TGT                 \
            --build=$(../build-aux/config.guess)

make -j$(nproc)
make DESTDIR=$LFS install -j1

cd ../..
rm -rf findutils-4.8.0

