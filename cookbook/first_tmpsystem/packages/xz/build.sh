tar xf $PKG_DIR/xz/xz-5.2.5.tar.xz
cd xz-5.2.5

mkdir -v build
cd build

../configure --prefix=/usr                        \
             --host=$LFS_TGT                      \
             --build=$(../build-aux/config.guess) \
             --disable-static                     \
             --docdir=/usr/share/doc/xz-5.2.5

make -j$(nproc)
make DESTDIR=$LFS install -j1

cd ../..
rm -rf xz-5.2.5

