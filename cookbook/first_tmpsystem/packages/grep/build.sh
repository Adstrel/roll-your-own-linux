tar xf $PKG_DIR/grep/grep-3.7.tar.xz
cd grep-3.7

mkdir -v build
cd build

../configure --prefix=/usr   \
            --host=$LFS_TGT


make -j$(nproc)
make DESTDIR=$LFS install -j1

cd ../..
rm -rf grep-3.7

