tar xf $PKG_DIR/gzip/gzip-1.10.tar.xz
cd gzip-1.10

mkdir -v build
cd build

../configure --prefix=/usr --host=$LFS_TGT

make -j$(nproc)
make DESTDIR=$LFS install -j1

cd ../..
rm -rf gzip-1.10

