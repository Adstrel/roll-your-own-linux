tar xf $PKG_DIR/sed/sed-4.8.tar.xz
cd sed-4.8

mkdir -v build
cd build

../configure --prefix=/usr   \
            --host=$LFS_TGT

make -j$(nproc)
make DESTDIR=$LFS install -j1

cd ../..
rm -rf sed-4.8

