tar xf $PKG_DIR/gcc/gcc-11.2.0.tar.xz

cd gcc-11.2.0

# Libraries that gcc needs
tar xf $PKG_DIR/mpfr/mpfr-4.1.0.tar.xz
mv -v mpfr-4.1.0 mpfr
tar xf $PKG_DIR/gmp/gmp-6.2.1.tar.xz
mv -v gmp-6.2.1 gmp
tar xf $PKG_DIR/mpc/mpc-1.2.1.tar.gz
mv -v mpc-1.2.1 mpc

# Default directory for 64-bit libraries should be 'lib'
# (otherwise the applications compiled with this gcc will
# not run because the dynamic libraries are installed in
# a different directory and the error is literaly just
# 'error: /bin/env not found' even though /bin/env is there,
# and in reality its the dynamic library thats missing)
sed -e '/m64=/s/lib64/lib/' \
    -i.orig gcc/config/i386/t-linux64

mkdir -v build
cd build

../configure                                       \
    --target=$LFS_TGT                              \
    --prefix=$LFS/tools                            \
    --with-glibc-version=$GLIBC_VERSION            \
    --with-sysroot=$LFS                            \
    --with-newlib                                  \
    --without-headers                              \
    --enable-initfini-array                        \
    --disable-nls                                  \
    --disable-shared                               \
    --disable-multilib                             \
    --disable-decimal-float                        \
    --disable-threads                              \
    --disable-libatomic                            \
    --disable-libgomp                              \
    --disable-libquadmath                          \
    --disable-libssp                               \
    --disable-libvtv                               \
    --disable-libstdcxx                            \
    --enable-languages=c,c++

make -j$(nproc)
make install -j1

cd ..
# The limits.h file was not created, lets create it with the
# same command GCC would normaly create it with
cat gcc/limitx.h gcc/glimits.h gcc/limity.h > \
    `dirname $($LFS/tools/bin/$LFS_TGT-gcc -print-libgcc-file-name)`/install-tools/include/limits.h

cd ..
rm -rf gcc-11.2.0

