tar xf $PKG_DIR/linux/linux-5.14.tar.xz
cd linux-5.14

make mrproper

make headers
find usr/include -name '.*' -delete
rm usr/include/Makefile
cp -rv usr/include $LFS/usr

cd ..
rm -rf linux-5.14

