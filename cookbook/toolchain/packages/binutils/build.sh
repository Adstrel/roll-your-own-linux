tar xf $PKG_DIR/binutils/binutils-2.37.tar.xz
cd binutils-2.37

mkdir -v build
cd build

../configure --prefix=$LFS/tools \
             --with-sysroot=$LFS \
             --target=$LFS_TGT   \
             --disable-nls       \
             --disable-werror

make -j$(nproc)
make install -j1

cd ../..
rm -rf binutils-2.37

