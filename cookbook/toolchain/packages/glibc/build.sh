tar xf $PKG_DIR/glibc/glibc-2.34.tar.xz
cd glibc-2.34

# Dynamic library loader needs this symlink
ln -sfv ld-linux-x86-64.so.2 $LFS/lib64/ld-lsb-x86-64.so.3

# Change the location where glibc stores runtime data so its FHS-compliant
patch -Np1 -i ../glibc-2.34-fhs-1.patch

mkdir -v build
cd build

# Programs ldconfig and sln should be installed into '/usr/sbin'
echo "rootsbindir=/usr/sbin" > configparms

../configure                           \
    --prefix=/usr                      \
    --host=$LFS_TGT                    \
    --build=$(../scripts/config.guess) \
    --enable-kernel=$LINUX_VERSION     \
    --with-headers=$LFS/usr/include    \
    --without-selinux                  \
    libc_cv_slibdir=/usr/lib

make -j$(nproc)
make DESTDIR=$LFS install -j1

sed '/RTLDLIST=/s@/usr@@g' -i $LFS/usr/bin/ldd

cd ../..
rm -rf glibc-2.34

# Run a GCC utility to generate a propper limits.h file
$LFS/tools/libexec/gcc/$LFS_TGT/11.2.0/install-tools/mkheaders

