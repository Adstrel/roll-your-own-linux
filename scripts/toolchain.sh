#!/bin/bash

# Set the environment
export COOKBOOK=cookbook/toolchain
source scripts/environment_setup.sh

# Build the Filesystem
mkdir -pv $LFS/{etc,var} $LFS/usr/{bin,lib} $LFS/tools
rm -fv $LFS/usr/sbin $LFS/usr/lib64 $LFS/bin $LFS/sbin $LFS/lib $LFS/lib64
ln -sv bin $LFS/usr/sbin
ln -sv lib $LFS/usr/lib64
ln -sv usr/bin $LFS/bin
ln -sv usr/bin $LFS/sbin
ln -sv usr/lib $LFS/lib
ln -sv usr/lib $LFS/lib64

exec bash scripts/build_packages.sh

