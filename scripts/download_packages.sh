#!/bin/bash

# Useful constants
ERROR="\033[1;31m"
RESET_COLOR="\033[0m"

# Change current directory to project's base directory
if [ `basename \`pwd\`` = "scripts" ]; then
    cd ..
fi

mkdir -p packages
pushd packages


# Read all non-empty lines
cat ../packages.txt | grep -v "^$" | while read line; do
    # Skip commented lines
    if [[ "$line" == \#* ]]; then
        continue
    fi

    pkg_name=`echo "$line" | cut --delimiter='>' --fields=1 | xargs`
    pkg_md5sum=`echo "$line" | cut --delimiter='>' --fields=2 | xargs`
    pkg_link=`echo "$line" | cut --delimiter='>' --fields=3 | xargs`
    pkg_filename=`basename $pkg_link`
    pkg_extension=`echo $pkg_filename | grep -o "\.[^0-9].*$"`

    mkdir -p $pkg_name
    pushd $pkg_name

    # Download the package only if it isn't already downloaded
    if [ ! -f "$pkg_filename" ]; then
        wget $pkg_link

        # Check if the downloaded file has correct md5sum
        if [ "$pkg_md5sum" ] && \
            [ "`md5sum $pkg_filename | cut --delimiter=' ' --fields=1`" != "$pkg_md5sum" ]; then

            printf "${ERROR}[ERROR]: Wrong md5sum for $pkg_filename${RESET_COLOR}\n"
            mv $pkg_filename WRONG_MD5SUM-$pkg_filename
        fi
    fi

    popd
done

popd

