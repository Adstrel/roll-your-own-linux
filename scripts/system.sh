#!/bin/bash
# Run this as root

# Set the environment
export COOKBOOK=cookbook/system
source scripts/environment_setup.sh

# Mount our directories
mkdir -pv $LFS/{scripts,packages,cookbook}
mount -v --bind scripts  $LFS/scripts
mount -v --bind packages $LFS/packages
mount -v --bind cookbook $LFS/cookbook

# Mount the Virtual Filesystems
mkdir -pv Root/{dev,proc,sys,run}
mknod -m 600 $LFS/dev/console c 5 1
mknod -m 666 $LFS/dev/null    c 1 3
mount -v --bind /dev $LFS/dev
mount -v --bind /dev/pts $LFS/dev/pts
mount -vt proc proc $LFS/proc
mount -vt sysfs sysfs $LFS/sys
mount -vt tmpfs tmpfs $LFS/run
if [ -h $LFS/dev/shm ]; then
      mkdir -pv $LFS/$(readlink $LFS/dev/shm)
fi

chroot "$LFS" /usr/bin/env -i       \
    HOME=/root                      \
    TERM="$TERM"                    \
    PS1='(lfs chroot) \u:\w\$ '     \
    PATH=/usr/bin                   \
    COOKBOOK="/$COOKBOOK"           \
    GLIBC_VERSION="$GLIBC_VERSION"  \
    LINUX_VERSION="$LINUX_VERSION"  \
    /bin/bash --login +h scripts/build_packages.sh

umount -v $LFS/scripts
umount -v $LFS/packages
umount -v $LFS/cookbook

# Mount the Virtual Filesystems
umount -v $LFS/dev/pts
umount -v $LFS/dev
umount -v $LFS/proc
umount -v $LFS/sys
umount -v $LFS/run

