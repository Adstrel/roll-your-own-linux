# This script is only run as part of the other scripts

ERROR="\033[1;31m"
RESET_COLOR="\033[0m"

# Build all the packages in set order
cat $COOKBOOK/order.txt | grep -v "^$" | while read pkg; do
    # Skip commented lines
    if [[ "$pkg" == \#* ]]; then
        continue
    fi

    export PKG_DIR=`realpath packages`
    pushd $COOKBOOK/packages/$pkg
    bash -e build.sh || (printf "${ERROR}Package \"$(basename $COOKBOOK)/$pkg\" failed to build!${RESET_COLOR}\n"; exit 1) || exit 1
    popd
done

