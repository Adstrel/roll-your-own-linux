# This script is only run as part of the other scripts

# Change current directory to project's base directory
if [ `basename $(pwd)` = "scripts" ]; then
    cd ..
fi

export LFS=`realpath Root`
export LFS_TGT=x86_64-lfs-linux-gnu
export PATH="$LFS/tools/bin:/usr/bin:/usr/sbin:/bin"
export LC_ALL=POSIX
export GLIBC_VERSION=`ls packages/glibc/ | grep "[0-9][0-9.]*[0-9]" --only-matching`

# Set the kernel version you want (must be equal or lower than your host version)
export LINUX_VERSION=5.4

# Turn off PATH hashing
set +h
# Default file permissions
umask 022

